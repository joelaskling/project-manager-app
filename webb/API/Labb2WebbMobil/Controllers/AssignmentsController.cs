﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labb2WebbMobil.Context;
using Labb2WebbMobil.Models;

namespace Labb2WebbMobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssignmentsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public AssignmentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        //// GET: api/Assignments
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Assignment>>> GetAssignments()
        //{
        //    return await _context.Assignments.ToListAsync();
        //}

      
        // POST: api/Assignments/ChangeResp

        [HttpPost("ChangeResp")]
        
        public async void ChangeResponsibility(Assignment assignment)
        {
            bool exists = false;

            foreach (var ass in _context.Assignments)
            {
                if(ass.Task.TaskId == assignment.Task.TaskId && ass.User.UserId == assignment.User.UserId)
                {
                    _context.Remove(ass);
                    exists = true;
                }
            }
            if (!exists)
            {
                _context.Add(new Assignment()
                {
                    Task = _context.Tasks.Find(assignment.Task.TaskId),
                    User = _context.Users.Find(assignment.User.UserId)
                });
            }
             _context.SaveChanges();
        }

        // Post: api/Assignments/GetUserResponsibility
        [HttpPost("GetUserResponsibility")]
        
        public async Task<ActionResult<IEnumerable<Models.Task>>> GetUserResponsibility(User user)
        {
            List<Models.Task> UserResponsibleTasks = new List<Models.Task>();

            foreach (var assignment in _context.Assignments)
            {
                if(user.UserId == assignment.User.UserId)
                {
                    UserResponsibleTasks.Add(assignment.Task);
                }
            }

            return UserResponsibleTasks;
        }

        // Post: api/Assignments/ResponsibleStatus
        [HttpPost("ResponsibleStatus")]
        public async Task<IEnumerable<string>> ResponsibleStatus(User user)
        {
            List<string> statusList = new List<string>();
            string status = "";
            bool found;
            int count;
            foreach (var task in _context.Tasks)
            {
                found = false;
                count = 0;
                foreach (var assignment in _context.Assignments)
                {
                    if (assignment.Task.TaskId == task.TaskId)
                    {
                        count++;
                        found = true;
                        if (user.UserId == assignment.User.UserId)
                        {
                            status = "user_responsible";
                        }
                        else
                        {
                            status = "other_responsible";
                        }
                    }
                }
                if (count >= 2)
                {
                    status = "too_many_responsible";
                }
                else if (!found)
                {
                    status = "no_responsible";
                }
                statusList.Add(status);
            }


            return statusList;
        }

        // POST: api/Assignments
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<List<User>>> PostAssignment(Models.Task task)
        {
            List<User> users = new List<User>();

            foreach (var assignment in _context.Assignments)
            {
                if(task.TaskId == assignment.Task.TaskId)
                {
                    users.Add(assignment.User);
                }
            }
            return users;
        }

    }
}
