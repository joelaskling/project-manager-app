﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labb2WebbMobil.Context;
using Labb2WebbMobil.Models;

namespace Labb2WebbMobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TasksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskUsers>>> GetTasks()
        {
            List<TaskUsers> tasks = new List<TaskUsers>();

            foreach (Models.Task task in _context.Tasks)
            {
                List<User> users = new List<User>();

                foreach (Assignment assignment in _context.Assignments)
                {
                    if(task.TaskId == assignment.Task.TaskId)
                    {
                        users.Add(assignment.User);
                    }
                }

                tasks.Add(new TaskUsers()
                {
                    TaskId = task.TaskId,
                    Requirements = task.Requirements,
                    BeginDateTime = task.BeginDateTime,
                    DeadLineDateTime = task.DeadLineDateTime,
                    Title = task.Title,
                    Users = users                   
                });
            }

            return tasks;
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Models.Task>> GetTask(int id)
        {
            var task = await _context.Tasks.FindAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return task;
        }

    }
}
