﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labb2WebbMobil.Context;
using Labb2WebbMobil.Models;

namespace Labb2WebbMobil.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // Post: api/Users/
        [HttpPost]
        public async Task<ActionResult<User>> AuthenticateUser(AuthenticateUser authenticateUser)
        {
            var id = from u in _context.Users where (u.Email == authenticateUser.Email) && (u.Password == authenticateUser.Password) select u.UserId;
            User user = _context.Users.Find(id.First());
            await _context.SaveChangesAsync();

            return user;
        }

    }
}
