﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labb2WebbMobil.Models
{
    public class AuthenticateUser
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Status { get; set; }
    }
}
