﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labb2WebbMobil.Models
{
    public class Assignment
    {
        public int AssignmentId { get; set; }
        public virtual Task Task { get; set; }
        public virtual User User { get; set; }
    }
}
