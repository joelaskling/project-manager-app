﻿using Labb2WebbMobil.Context;
using Labb2WebbMobil.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labb2WebbMobil.DbSeed
{
    public class Seeder
    {
        static User user1;
        static User user2;
        static Models.Task task1;
        static Models.Task task2;
        static Models.Task task3;
        static Models.Task task4;
        static Assignment assignment1;
        static Assignment assignment2;
        static Assignment assignment3;
        static Assignment assignment4;

        public void PopulateDb(ApplicationDbContext context)
        {
            user1 = new User()
            {
               FirstName = "Kalle",
               LastName = "Johansson",
               Email = "kallejohansson@gmail.com",
               Password="kalle"
            };

            user2 = new User()
            {
                FirstName = "Nalle",
                LastName = "Johansson",
                Email = "nallejohansson@gmail.com",
                Password = "nalle"
            };

            task1 = new Models.Task()
            {
                Title = "Webbsida 1",
                BeginDateTime = DateTime.Now.ToString(),
                DeadLineDateTime = DateTime.Now.ToString(),
                Requirements = "Kalle skall göra en webbsida."
            };
            task2 = new Models.Task()
            {
                Title = "Webbsida 2",
                BeginDateTime = DateTime.Now.ToString(),
                DeadLineDateTime = DateTime.Now.ToString(),
                Requirements = "Nalle skall göra en webbsida"
            };
            task3 = new Models.Task()
            {
                Title = "Webbsida 3",
                BeginDateTime = DateTime.Now.ToString(),
                DeadLineDateTime = DateTime.Now.ToString(),
                Requirements = "Det skall göras en webbsida"
            };
            task4 = new Models.Task()
            {
                Title = "Webbsida 4",
                BeginDateTime = DateTime.Now.ToString(),
                DeadLineDateTime = DateTime.Now.ToString(),
                Requirements = "Vem skall göra en webbsida?"
            };
            assignment1 = new Assignment()
            {
                Task = task1,
                User = user1
            };
            assignment2 = new Assignment()
            {
                Task = task2,
                User = user2
            };
            assignment3 = new Assignment()
            {
                Task = task3,
                User = user1
            };
            assignment4 = new Assignment()
            {
                Task = task3,
                User = user2
            };

            context.Users.Add(user1);
            context.Users.Add(user2);
            context.Assignments.Add(assignment1);
            context.Assignments.Add(assignment2);
            context.Assignments.Add(assignment3);
            context.Assignments.Add(assignment4);
            context.Tasks.Add(task1);
            context.Tasks.Add(task2);
            context.Tasks.Add(task3);
            context.Tasks.Add(task4);
        }
     
    }
}
