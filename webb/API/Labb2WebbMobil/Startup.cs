using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Labb2WebbMobil.Context;
using Labb2WebbMobil.DbSeed;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Labb2WebbMobil
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                Seeder seed = new Seeder();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                seed.PopulateDb(context);
                context.SaveChanges();
            }

            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=ProjectDb;Trusted_Connection=True;MultipleActiveResultSets=true;"));


            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
