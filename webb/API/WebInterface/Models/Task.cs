﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebInterface.Models
{
    public class Task
    {
        public int TaskId { get; set; }
        public string BeginDateTime { get; set; }
        public string DeadLineDateTime { get; set; }
        public string Title { get; set; }
        public string Requirements { get; set; }
        public List<User> Users { get; set; }
    }
}
