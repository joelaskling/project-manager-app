package com.example.labb2webbochmobil.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable {
    private int taskId;
    private String beginDateTime;
    private String deadLineDateTime;
    private String title;
    private String requirements;

    protected Task(Parcel in) {
        taskId = in.readInt();
        beginDateTime = in.readString();
        deadLineDateTime = in.readString();
        title = in.readString();
        requirements = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public int getTaskId() {
        return taskId;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public String getDeadLineDateTime() {
        return deadLineDateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public void setDeadLineDateTime(String deadLineDateTime) {
        this.deadLineDateTime = deadLineDateTime;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(taskId);
        dest.writeString(beginDateTime);
        dest.writeString(deadLineDateTime);
        dest.writeString(title);
        dest.writeString(requirements);
    }
}
