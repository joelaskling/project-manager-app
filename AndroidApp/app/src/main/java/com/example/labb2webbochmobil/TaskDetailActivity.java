package com.example.labb2webbochmobil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.labb2webbochmobil.Model.Assignment;
import com.example.labb2webbochmobil.Model.Task;
import com.example.labb2webbochmobil.Model.User;
import com.example.labb2webbochmobil.Service.ApiInterface;
import com.example.labb2webbochmobil.Service.RetrofitClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskDetailActivity extends AppCompatActivity {
    private Task task;
    private User user;
    private boolean userResponsible;
    private TextView title;
    private TextView requirements;
    private TextView startDate;
    private TextView endDate;
    private ListView responsibleList;
    private Button responsibleButton;
    ArrayAdapter arrayAdapter;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);
        Gson gson = new Gson();
        String taskString = getIntent().getStringExtra("EXTRA_TASK");
        task = gson.fromJson(taskString, Task.class);
        userResponsible = false;
        count = 0;

        title = (TextView) findViewById(R.id.DetailsTitleText);
        requirements = (TextView) findViewById(R.id.DetailsRequirementText);
        startDate = (TextView) findViewById(R.id.DetailsStartDateText);
        endDate = (TextView) findViewById(R.id.DetailsEndDateText);
        responsibleList = (ListView) findViewById(R.id.DetailsResponsibleList);
        responsibleButton = (Button) findViewById(R.id.DetailsResponsibleButton);

        title.setText(task.getTitle());
        requirements.setText(task.getRequirements());
        startDate.setText(task.getBeginDateTime());
        endDate.setText(task.getDeadLineDateTime());

        user = gson.fromJson(getIntent().getStringExtra("EXTRA_SESSION_USER"), User.class);
        LoadResponsible();

        responsibleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeResponsibility();
            }
        });

    }

    private void ChangeResponsibility(){
        ApiInterface s = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
        Assignment assignment = new Assignment(task, user);
        Call<Void> c = s.changeResp(assignment);

        c.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                LoadResponsible();
                if(userResponsible){
                    responsibleButton.setText("Take Responsibility");
                }else{
                    responsibleButton.setText("Reject Responsibility");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Could not connect to the server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void LoadResponsible(){
        ApiInterface service = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
        Call<List<User>> call = service.getUsers(task);

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                ArrayList<User> users;
                if(!response.body().isEmpty()){
                    List<String> userNames = new ArrayList<String>();
                    users = new ArrayList<User>(response.body());

                    for (User u : users)
                    {
                        userNames.add(u.getFirstName() + " " + u.getLastName());
                    }

                    arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, userNames);
                    responsibleList.setAdapter(arrayAdapter);
                    ResponsibilityStatus(users);
                }else{
                    List<String> noResp = new ArrayList<String>();
                    noResp.add("No responsible person");
                    arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, noResp);
                    responsibleList.setAdapter(arrayAdapter);
                    userResponsible = false;
                }
                if(count == 0){
                    if(userResponsible){
                        responsibleButton.setText("Reject Responsibility");
                    }else{
                        responsibleButton.setText("Take Responsibility");
                    }
                }
                count++;
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Could not connect to the server", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ResponsibilityStatus(ArrayList<User> users){
        userResponsible = false;

        for (User u : users)
        {
            if(u.getUserId() == user.getUserId()){
                userResponsible = true;
            }
        }
    }
}
