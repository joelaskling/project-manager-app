package com.example.labb2webbochmobil.Model;

public class AuthenticateUser {
    private String Email;
    private String Password;

    public AuthenticateUser(String email, String password) {
        Email = email;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
