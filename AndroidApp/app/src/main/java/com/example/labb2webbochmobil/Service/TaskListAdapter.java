package com.example.labb2webbochmobil.Service;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.labb2webbochmobil.Model.Assignment;
import com.example.labb2webbochmobil.Model.Task;
import com.example.labb2webbochmobil.Model.User;
import com.example.labb2webbochmobil.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskListAdapter extends ArrayAdapter<Task>{
    private static final String TAG = "PersonListAdapter";
    private Context mContext;
    private Intent mIntent;
    private Task currentTask;
    private Task changeRespTask;
    private User user;

    private ArrayList<Task> tasks;
    private ArrayList<Task> userResponsibleTasks;
    private ArrayList<String> responsibleStatus;

    private static class ViewHolder{
        TextView titleText;
        TextView startDateText;
        TextView endDateText;
        CheckBox respCheckBox;
        ProgressBar responsibilityCircle;
    }

    public TaskListAdapter(ArrayList<Task> data, Context context, Intent intent, ArrayList<Task> userTasks, ArrayList<String> responsibleStatus){
        super(context, R.layout.adapter_view_layout, data);
        tasks = data;
        userResponsibleTasks = userTasks;
        mContext = context;
        mIntent = intent;
        this.responsibleStatus=responsibleStatus;

    }

    public void addUserTasks(List<Task> userTasks) {
        userResponsibleTasks.clear();
        userResponsibleTasks.addAll(userTasks);
    }

    public void addResponsibleStatus(List<String > respStatus) {
        responsibleStatus.clear();
        responsibleStatus.addAll(respStatus);
    }

    private int lastPosition = -1;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        currentTask = getItem(position);
        ViewHolder viewHolder;

        final View result;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_view_layout, parent, false);

            viewHolder.titleText = (TextView) convertView.findViewById(R.id.TitleText);
            viewHolder.startDateText = (TextView) convertView.findViewById(R.id.StartDateText);
            viewHolder.endDateText = (TextView) convertView.findViewById(R.id.EndDateText);
            viewHolder.respCheckBox = (CheckBox) convertView.findViewById(R.id.respCheckBox );
            viewHolder.responsibilityCircle = (ProgressBar) convertView.findViewById(R.id.progressBar);

            Gson gson = new Gson();
            user = gson.fromJson(mIntent.getStringExtra("EXTRA_SESSION_USER"), User.class);
            viewHolder.respCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.isPressed()){
                        for (Task t:tasks) {
                            if(t.getTaskId()==Integer.parseInt(buttonView.getTag().toString())){
                                changeRespTask = t;
                            }
                        }

                        ApiInterface s = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
                        Assignment assignment = new Assignment(changeRespTask, user);
                        Call<Void> call = s.changeResp(assignment);
                        call.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                            }
                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                            }
                        });
                    }
                }
        });

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        lastPosition = position;

        viewHolder.titleText.setText(currentTask.getTitle());
        viewHolder.startDateText.setText(currentTask.getBeginDateTime());
        viewHolder.endDateText.setText(currentTask.getDeadLineDateTime());
        viewHolder.respCheckBox.setTag(currentTask.getTaskId());
        viewHolder.respCheckBox.setChecked(false);

        if(!responsibleStatus.isEmpty()){
            switch(responsibleStatus.get(position)){
                case "user_responsible":
                    viewHolder.responsibilityCircle.setProgressTintList(ColorStateList.valueOf(Color.GREEN));
                    break;
                case "other_responsible":
                    viewHolder.responsibilityCircle.setProgressTintList(ColorStateList.valueOf(Color.YELLOW));
                    break;
                case "too_many_responsible":
                    viewHolder.responsibilityCircle.setProgressTintList(ColorStateList.valueOf(Color.RED));
                    break;
                case "no_responsible":
                    viewHolder.responsibilityCircle.setProgressTintList(ColorStateList.valueOf(Color.GRAY));
                    break;
            }
        }


        if(userResponsibleTasks !=null){
            for (Task t: userResponsibleTasks) {
                if(t.getTaskId()== currentTask.getTaskId()){
                    viewHolder.respCheckBox.setChecked(true);
                }
            }
        }

        return convertView;
    }





}
