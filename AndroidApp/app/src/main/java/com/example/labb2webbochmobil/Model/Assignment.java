package com.example.labb2webbochmobil.Model;

public class Assignment {
    private int AssignmentId;
    private Task Task;
    private User User;

    public Assignment(com.example.labb2webbochmobil.Model.Task task, com.example.labb2webbochmobil.Model.User user) {
        Task = task;
        User = user;
    }

    public int getAssignmentId() {
        return AssignmentId;
    }

    public com.example.labb2webbochmobil.Model.Task getTask() {
        return Task;
    }

    public com.example.labb2webbochmobil.Model.User getUser() {
        return User;
    }


}
