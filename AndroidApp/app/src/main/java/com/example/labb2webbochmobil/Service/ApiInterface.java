package com.example.labb2webbochmobil.Service;

import com.example.labb2webbochmobil.Model.Assignment;
import com.example.labb2webbochmobil.Model.AuthenticateUser;
import com.example.labb2webbochmobil.Model.Task;
import com.example.labb2webbochmobil.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ApiInterface {
    @POST("api/Users")
    Call<User> performUserLogin(@Body AuthenticateUser authenticateUser);

    @GET("api/Tasks")
    Call<List<Task>> getTasks();

    @POST("api/Assignments")
    Call<List<User>> getUsers(@Body Task task);

    @POST("api/Assignments/ChangeResp")
    Call<Void> changeResp(@Body Assignment assignment);

    @POST("api/Assignments/GetUserResponsibility")
    Call<List<Task>> getUserResp(@Body User user);

    @POST("api/Assignments/ResponsibleStatus")
    Call<List<String>> responsibleStatus(@Body User user);

}

