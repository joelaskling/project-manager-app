package com.example.labb2webbochmobil;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.labb2webbochmobil.Model.AuthenticateUser;
import com.example.labb2webbochmobil.Model.User;
import com.example.labb2webbochmobil.Service.ApiInterface;
import com.example.labb2webbochmobil.Service.RetrofitClient;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {
    private EditText email;
    private EditText password;
    private TextView errorMsg;
    private Button SignInBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = (EditText)findViewById(R.id.EmailEntry);
        password = (EditText)findViewById(R.id.PasswordEntry);
        errorMsg = (TextView)findViewById(R.id.ErrorTextField);
        SignInBtn = (Button)findViewById(R.id.SignInButton);


        SignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }
    private void login(){
        ApiInterface service = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);
        AuthenticateUser login = new AuthenticateUser(email.getText().toString(), password.getText().toString());
        Call<User> call = service.performUserLogin(login);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                if(user == null){
                    Toast.makeText(MainActivity.this, "Incorrect email or password...", Toast.LENGTH_SHORT).show();
                }else{
                    Gson gson = new Gson();
                    Intent intent = new Intent(getApplicationContext(), MainPageActivity.class);
                    intent.putExtra("EXTRA_SESSION_USER", gson.toJson(user));
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
