package com.example.labb2webbochmobil;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.labb2webbochmobil.Model.Task;
import com.example.labb2webbochmobil.Model.User;
import com.example.labb2webbochmobil.Service.ApiInterface;
import com.example.labb2webbochmobil.Service.RetrofitClient;
import com.example.labb2webbochmobil.Service.TaskListAdapter;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainPageActivity extends AppCompatActivity {
    private ListView listView;
    ArrayList<Task> tasks;
    ArrayList<Task> userTasks;
    ArrayList<String> responsibleStatus;
    private static TaskListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        listView = (ListView)findViewById(R.id.Assignments);

        tasks=new ArrayList<Task>();
        userTasks=new ArrayList<Task>();
        responsibleStatus=new ArrayList<String>();

        adapter = new TaskListAdapter(tasks, getApplicationContext(), getIntent(), userTasks, responsibleStatus);
        listView.setAdapter(adapter);

        getAllTasks();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), TaskDetailActivity.class);
                Gson gson = new Gson();
                User user = gson.fromJson(getIntent().getStringExtra("EXTRA_SESSION_USER"), User.class);
                intent.putExtra("EXTRA_SESSION_USER", gson.toJson(user));
                intent.putExtra("EXTRA_TASK", gson.toJson(tasks.get(position)));
                startActivity(intent);
            }

        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        getAllTasks();

    }

    public void getResponsibleStatus()
    {
        ApiInterface service = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);

        Gson gson = new Gson();
        User user = gson.fromJson(getIntent().getStringExtra("EXTRA_SESSION_USER"), User.class);
        Call<List<String>> call = service.responsibleStatus(user);
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if(response.isSuccessful()){
                    responsibleStatus=new ArrayList<String>(response.body());
                    adapter.addResponsibleStatus(responsibleStatus);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        });
    }

    public void getAllTasks()
    {
        ApiInterface service = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);

        Gson gson = new Gson();
        User user = gson.fromJson(getIntent().getStringExtra("EXTRA_SESSION_USER"), User.class);

        Call<List<Task>> call1 = service.getUserResp(user);
        call1.enqueue(new Callback<List<Task>>() {
            @Override
            public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                if(response.isSuccessful()){
                    userTasks = new ArrayList<Task>(response.body() );
                    adapter.addUserTasks(userTasks);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Task>> call, Throwable t) {

            }
        });

        Call<List<Task>> call = service.getTasks();
        call.enqueue(new Callback<List<Task>>() {
            @Override
            public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                if(response.isSuccessful()){

                    tasks = new ArrayList<Task>(response.body() );
                    adapter.clear();
                    adapter.addAll(tasks);
                    adapter.notifyDataSetChanged();
                    getResponsibleStatus();



                }else{
                    Toast.makeText(MainPageActivity.this, "There is no tasks at the moment...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Task>> call, Throwable t) {
                Toast.makeText(MainPageActivity.this, "No server connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
